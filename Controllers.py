from uuid import uuid4
from flask import Flask, jsonify, request, json
import Blockchains

app = Flask(__name__)

blockChain = Blockchains.blockChains()


#EndPoint to handle transactions
@app.route('/transactions/new', methods=['POST'])
def newTransaction(values=None):
    values = request.get_json()

    #Check if the body has the required fields
    required = ['sender', 'receiver', 'amount' ]
    if not all(k in values for k in required):
        return 'Check the body', 400

    result = blockChain.newTransaction(
        sender=values['sender'],
        receiver=values['receiver'],
        amount=values['amount']
    )

    if result == 0 :
        message = {'message': 'Transaction Complete'}
        return jsonify(message), 200
    else:
        message = {'message': 'Error During transaction'}
        return jsonify(message), 400

# with app.test_client() as c:
#     result = c.post('/transactions/new')
#     data = json.loads(result.data)
#     assert 200 in data


#Get all the transactions
@app.route('/transactions/all', methods=['GET'])
def allTransactions():

    result = blockChain.allTransactions()

    if len(result) <= 0:
        message = {'message' : 'No Transactions'}
        return jsonify(message), 200
    else :
        return jsonify(result), 200


#Get all the transactions for a user
@app.route('/transactions/<user>', methods=['GET'])
def userTransactions(user):
    result = blockChain.userTransactions(user)

    if len(result) <= 0:
        message = {'message' : 'No Transactions for the user'}
        return jsonify(message), 200
    else :
        return jsonify(result), 200



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)


