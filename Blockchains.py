from time import time
import json
import hashlib

class blockChains(object):
    def __init__(self):
        #Initiate the block chains and the current transactions
        self.chain = []
        self.currentTransactions = []

    #Function to create a new block
    def addBlock(self):
        # Build the block for the block chain
        if len(self.chain) <= 0:
            block = {
                'index': len(self.chain) + 1,
                'timeStamp': time(),
                'transactions': self.currentTransactions,
                'previousBlock': 0
            }
        else:
            block = {
                'index': len(self.chain) + 1,
                'timeStamp': time(),
                'transactions': self.currentTransactions,
                'previousBlock': self.hash(self.chain[-1]),
            }

        # Empty the current transactions after creating a block
        self.currentTransactions = []

        # Add the block to the end of the chain
        self.chain.append(block)

        return block

    #Function to create a new transaction
    def newTransaction(self, sender, receiver, amount):
        try:
            #Creating a transaction
            transaction = {
                'sender': sender,
                'receiver': receiver,
                'amount': amount
            }

            #Append the transactions to the list of transactions
            self.currentTransactions.append(transaction)

            #Add a new block to the chain
            self.addBlock()

            return 0
        except :
            return -1

    #Function to get all the transaction
    def allTransactions(self):
        # Variable for all the transactions
        transactions = []
        try:

            #Check for the length of the chain
            if len(self.chain) > 0 :
                #For every block in the chain
                for block in self.chain:
                    for transaction in block['transactions']:
                        #Get the transaction in the list to return
                        transactions.append(transaction)
            else:
                #There chain is empty so raise an error
                raise ValueError
            return transactions
        except ValueError:
            return transactions

    #Function to get all the transaction of a user
    def userTransactions(self, user):
        #Variable for all the transactions for a user
        transactions = []
        try:
            #Go through the chain
            if len(self.chain) > 0:
                for block in self.chain:
                    for transaction in block['transactions']:
                        #For every transaction in the chain, check if the sender or the receiver name is equal to the user
                        if(transaction['sender'] == user or transaction['receiver'] == user):
                            transactions.append(block)
                            break
            else:
                #The chain is empty or there are no transactions for this specific user
                raise ValueError
            return transactions
        except ValueError :
            return transactions




    #Function to hash the previous block
    @staticmethod
    def hash(block):
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()