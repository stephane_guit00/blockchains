from flask import Flask, jsonify, request, json
import Controllers
import unittest

app = Flask(__name__)

with app.test_client() as c:
    result = c.post('/transactions/new')
    data = json.loads(result.data)
    result = Controllers.newTransaction(c)
    result = app.process_response(result)

    assert 200 in result


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)